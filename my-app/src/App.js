import './App.css';
import {BrowserRouter as Router,Switch,Route} from "react-router-dom";
import Home from './components/home/home'
function App() {
  return (
<Router>
      <Switch>
        <Route exact={true} path="/" component={Home}  />
      </Switch>
    </Router>
  );
}

export default App;
